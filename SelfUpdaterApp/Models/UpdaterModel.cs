﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using SelfUpdaterLib;
using System.Threading;
using System.Configuration;
using System.IO;
using Microsoft.Practices.Unity;
using System.Windows;
using System.Diagnostics;

namespace SelfUpdaterApp 
{
    public class UpdaterModel : IUpdaterModel
    {
        private IRemoteVersionHelper versionHelper;
        private bool updatesReady;
        private UpdatesDownloader downloader;
        private CancellationTokenSource _cancelationTokenSource;
        private int delay;
        private IUpdatable info;
        private UpdateInfo updInfo;
        private Task listener;
        private CancellationTokenSource cancellationTokenSource;

        public event EventHandler UpdateReadyEvt;
        public UpdaterModel(IRemoteVersionHelper versionHelper)
        {
            this.versionHelper = versionHelper;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["PollingTimerMsec"], out delay))
            {
                delay = 500;
            }
            downloader = new UpdatesDownloader();
            downloader.DownloadComplete += downloader_DownloadComplete;
            updatesReady = false;

            cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;
            listener = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    CheckUpdates();
                    Thread.Sleep(delay);
                    if (token.IsCancellationRequested)
                        break;
                }

                //cleanup
            }, token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        [Dependency]
        public IUpdatable Info
        {
            get { return info; }
            set { info = value; }
        }

        public bool UpdateReady
        {
            get
            {
                return updatesReady;
            }
            private set
            {
                updatesReady = value;
                UpdateReadyEvt(this, null);
            }
        }

        public void CheckUpdates()
        {
            if (versionHelper.UpdateInfoPublished)
            {
                updInfo = versionHelper.ParseUpdateInfo() ?? new UpdateInfo(info.Version, null);

                if ((info.Version < updInfo.Version) && (updInfo.AssemblyUri != null))
                {
                    downloader.Download(updInfo.AssemblyUri);
                    cancellationTokenSource.Cancel();
                }
            }
        }

        public void DoUpdate()
        {
            string currentPath = info.Assembly.Location;
            string filename;
            filename = System.IO.Path.GetFileName(updInfo.AssemblyUri.LocalPath);
            string newPath = Path.GetDirectoryName(currentPath) + "\\" + filename;
            UpdateApplication(downloader.TempFile, currentPath, newPath);

            Application.Current.Shutdown();

        }

        private void downloader_DownloadComplete(object sender, EventArgs e)
        {
            UpdateReady = true;            
        }

        private void UpdateApplication(string tempFile, string currentPath, string newPath)
        {
            string magic = "/C Choice /C Y /N /D Y /T 4 & Del /F /Q \"{0}\" & Choice /C Y /N /D Y /T 2 & Move /Y \"{1}\" \"{2}\" & Start \"\" /D \"{3}\" \"{4}\"";

            ProcessStartInfo pInfo = new ProcessStartInfo("cmd");
            pInfo.Arguments = string.Format(magic, currentPath, tempFile, newPath, Path.GetDirectoryName(newPath), Path.GetFileName(newPath));
            pInfo.WindowStyle = ProcessWindowStyle.Hidden;
            pInfo.CreateNoWindow = true;
            Process.Start(pInfo);
        }

    }
}
