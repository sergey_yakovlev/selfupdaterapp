﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfUpdaterLib
{
    public interface IRemoteVersionHelper
    {
        //Check whether the update info is published 
        bool UpdateInfoPublished { get; }

        //Obviously parses info about the update on the server
        UpdateInfo ParseUpdateInfo();

        //App info?

    }
}
