﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SelfUpdaterLib;
using System.Reflection;

namespace SelfUpdaterApp
{
    public class AppInfo : IUpdatable
    {
        public string Name
        {
            get { return "SelfUpdaterApp"; }
        }

        public System.Reflection.Assembly Assembly
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public Version Version
        {
            get { return new Version(1,0,0,0); }
        }
    }


}
