﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.Unity;
using SelfUpdaterLib;

namespace SelfUpdaterApp
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IUnityContainer container = new UnityContainer();
            var info = new AppInfo();     
            container.RegisterInstance<IUpdatable>(info);
            IRemoteVersionHelper remoteVersionHelper = container.Resolve<RemoteVersionHelperXML>();
            container.RegisterInstance<IRemoteVersionHelper>(remoteVersionHelper);

            var window = container.Resolve<MainWindow>();
            window.Show();
        }
    }
}
