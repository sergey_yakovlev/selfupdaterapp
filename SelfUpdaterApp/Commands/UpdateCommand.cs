﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SelfUpdaterApp.Commands
{
    public class UpdateCommand : ICommand
    {
        private UpdaterViewModel vm;

        public UpdateCommand(UpdaterViewModel vm)
        {
            this.vm = vm;
        }

        public bool CanExecute(object parameter)
        {
            return vm.CanUpdate();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
            
        public void Execute(object parameter)
        {
            string sMessageBoxText = "Do you want to restart and update?";
            string sCaption = "Update is ready";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNo;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            switch (rsltMessageBox)
            {
                case MessageBoxResult.Yes:
                    vm.DoUpdate();
                    break;

                case MessageBoxResult.No:
                    break;
            }
            
        }
    }
}
