﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SelfUpdaterLib;
using System.Net;
using System.Xml.Linq;
using System.IO;
using System.ComponentModel;
using Microsoft.Practices.Unity;
using System.Configuration;
using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;


namespace SelfUpdaterApp
{
    class RemoteVersionHelperXML : IRemoteVersionHelper
    {
        private IUpdatable info;
        private Uri updateFileUri;
        private CloudStorage dropBox;

        public RemoteVersionHelperXML()
        {
            updateFileUri = new Uri(ConfigurationManager.AppSettings["UpdateInfoUri"]);
        }

        [Dependency]
        public AppInfo Info
        {
            set { info = value; }
        }

        public bool UpdateInfoPublished
        {
            get 
            {
                try
                {
                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(updateFileUri.AbsoluteUri);
                    using (HttpWebResponse webResp = (HttpWebResponse)webReq.GetResponse())
                    {
                        return webResp.StatusCode == HttpStatusCode.OK;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        public UpdateInfo ParseUpdateInfo()
        {
            try
            {
                WebRequest request = HttpWebRequest.Create(updateFileUri.OriginalString);

                using (WebResponse response = request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                {
                    var doc = XDocument.Load(stream);

                    Version version = new Version(doc.Root.Element(info.Name).Element("Version").Value);
                    Uri assemblyUri = new Uri(doc.Root.Element(info.Name).Element("Uri").Value);

                    return new UpdateInfo(version, assemblyUri);
                }
                
            }
            catch
            {
                return null;
            }
        }
    }
}
