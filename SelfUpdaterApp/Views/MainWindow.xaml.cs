﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SelfUpdaterApp.Commands;
using SelfUpdaterApp.Properties;
using Microsoft.Practices.Unity;

namespace SelfUpdaterApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UpdaterViewModel vm;

        [Dependency]
        public UpdaterViewModel VM
        {
            set
            {
                vm = value;
                this.DataContext = vm;
            }
        }


        public MainWindow()
        {
            InitializeComponent();            
        }

    }
}
