﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfUpdaterLib
{
    public class UpdateInfo
    {
        public Uri AssemblyUri { get; private set; }
        public Version Version { get; private set; }

        public UpdateInfo(Version ver, Uri uri)
        {
            AssemblyUri = uri;
            Version = ver;
        }

         
    }
}
