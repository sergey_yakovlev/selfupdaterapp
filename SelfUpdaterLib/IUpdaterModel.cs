﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfUpdaterLib
{
    public interface IUpdaterModel
    {
        event EventHandler UpdateReadyEvt;
        bool UpdateReady { get; }
        void DoUpdate();
    }
}
