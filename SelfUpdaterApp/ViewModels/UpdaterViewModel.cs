﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;
using SelfUpdaterApp.Commands;
using SelfUpdaterLib;

namespace SelfUpdaterApp
{
    public class UpdaterViewModel : DependencyObject
    {
        private UpdaterModel updater;

        public ICommand UpdateCommand { get; set; }

        public UpdaterViewModel(UpdaterModel model)
        {
            updater = model;
            updater.UpdateReadyEvt += updater_UpdateReadyEvt;
            this.UpdateCommand = new UpdateCommand(this);
            Version = model.Info.Version.ToString();
        }

        public string Version
        {
            get
            {
                return (string)GetValue(VersionProperty); 
            }

            private set
            {
                SetValue(VersionProperty, value);
            }
        }

        public bool UpdateReady
        {
            get
            {
                return (bool)GetValue(UpdateReadyProperty); 
            } 
            private set
            {
                Dispatcher.Invoke(
                        System.Windows.Threading.DispatcherPriority.Normal,
                        new Action(
                        delegate()
                        {
                            SetValue(UpdateReadyProperty, value);
                            CommandManager.InvalidateRequerySuggested();
                        }));
            }
           
        }

        public static readonly DependencyProperty UpdateReadyProperty =
            DependencyProperty.Register("UpdateReady", typeof(bool), typeof(UpdaterViewModel), new UIPropertyMetadata(false));

        public static readonly DependencyProperty VersionProperty =
            DependencyProperty.Register("Version", typeof(string), typeof(UpdaterViewModel), new UIPropertyMetadata("0.0.0.0"));

        
        void updater_UpdateReadyEvt(object sender, EventArgs e)
        {
            IUpdaterModel mdl = (IUpdaterModel) sender;
            UpdateReady = mdl.UpdateReady;
        }

        public void DoUpdate()
        {
            //prompt?
            updater.DoUpdate();
        }

        public bool CanUpdate()
        {
            return UpdateReady;
        }
    }
}
