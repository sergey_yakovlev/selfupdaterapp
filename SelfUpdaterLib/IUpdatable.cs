﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace SelfUpdaterLib
{
    public interface IUpdatable
    {
        //App name
        string Name { get; }

        //Required to pick the right file to update and restart. 
        Assembly Assembly { get; }

        //Actually running version
        Version Version { get; }
    }
}
