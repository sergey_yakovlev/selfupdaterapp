﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Net;
using System.ComponentModel;
using System.IO;
using Microsoft.Practices.Unity;
using System.Configuration;

namespace SelfUpdaterApp
{
    public class UpdatesDownloader
    {
        private WebClient webClient;

        public string TempFile { get; private set; }
        public event EventHandler DownloadComplete;

        public UpdatesDownloader()
        {
            TempFile = Path.GetTempFileName();

            webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(webClient_DownloadFileCompleted);
        }

        public void Download(Uri uri)
        {
            try
            {
                webClient.DownloadFileAsync(uri, TempFile);

            }
            catch
            {
                //some handling
            }
        }

        void webClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                 DownloadComplete(this, null);                
            }
            else
            {
                //some handling
            }
        }
    }
}
